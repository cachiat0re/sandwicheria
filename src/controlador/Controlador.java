/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Pedido;
import modelo.Producto;
import modelo.Repo;
import modelo.Rubro;
import modelo.Sandwicheria;
import org.datacontract.schemas._2004._07.sge_service_contracts.Autorizacion;
import vista.CerrarCaja;
import vista.Login;
import vista.NuevoPedido;
import vista.VPrincipal;


/**
 *
 * @author gabriel
 */
public class Controlador {
    //manejea las vistas y modelo
    private Login vlogin;
    private VPrincipal vprincipal;
    private Sandwicheria modelo;
    private NuevoPedido vpedido;
    private Producto producto;
    private Pedido pedido;
    private CerrarCaja vcaja;

    public Controlador() {
        modelo = new Sandwicheria();
        vprincipal = new VPrincipal(this);
        vlogin = new Login(vprincipal,this);
        vpedido = new NuevoPedido(vprincipal,this);
        vcaja = new CerrarCaja(vprincipal);
        pedido = new Pedido();
       
        //agregar las otras ventanas
        
        
    }
    
    public void ejecutar(){
        vprincipal.setVisible(true);
        mostrarNumeroPedido();
        vlogin.setVisible(true);
    }

    public void cerrar() {
        System.exit(0); //todo se cierra
    }

    public void ingresar() {
        String usuario = vlogin.getUsuario();
        String clave = vlogin.getClave();
        Boolean h = modelo.login(usuario, clave);
        if(h){
            vlogin.setVisible(false);
        }
        else{
            JOptionPane.showMessageDialog(vlogin, "Usuario no reconocido");
        }
    }

    public void abrirPedido() {
        vpedido.ponerRubros(Repo.rubros);
        ponerAgregados();
        vpedido.setVisible(true);
    }

    public void ponerProductos() {
        Rubro aux=Repo.rubros.get(vpedido.getRubro());
        vpedido.ponerProductos(aux.getProductos());
        
    }
    
    public void ponerAgregados(){
        vpedido.ponerAgregados(Repo.agregados);
    }

    public void insertarAgregado(int selectedIndex) {
        
        producto = modelo.crearProducto(producto,vpedido.getRubro(),vpedido.getProducto());
        producto.agregar(Repo.agregados.get(selectedIndex));
        vpedido.mostrarAgregado(producto.getAgregados());
    }

    public void agregarProducto() {
        
        if(producto==null){
           producto = modelo.crearProducto(producto,vpedido.getRubro(),vpedido.getProducto());
        }
        
        if(producto.getExistencia()<1){
            JOptionPane.showMessageDialog(vcaja, "No hay existencia");
            
            //limpiamos el producto
            producto=null;
        }
        else{
            pedido.agregarProducto(producto);
            mostrarPedido();
            vpedido.limpiarTablaAgregados();
            producto=null;
        }
    }

    private void mostrarPedido() {
         DefaultTableModel matriz = new DefaultTableModel();
         matriz.addColumn("Descripcion");
         matriz.addColumn("Precio");
         matriz.addColumn("Precio con agregados");
         matriz.addColumn("sub total");
         
         
         for(Producto p: pedido.getProductos()){
             matriz.addRow(new Object[]{p.getNombre(),p.getPrecio(),p.calcularTotalAgregados(),p.calcularTotal()});
         }
         
         vpedido.mostrarPedido(matriz);
         vpedido.mostrarTotal(pedido.calcularTotal());
    }

    public void guardarPedido() {
        int res = JOptionPane.showConfirmDialog(vlogin, "desea guardar este pedido","Confirme",JOptionPane.YES_NO_OPTION);
        if(res == JOptionPane.YES_OPTION){
            pedido.setCajero(Sandwicheria.activo);//agregamos el cajero al pedido
            Repo.pedidos.add(pedido);
            modelo.descontarExistencias(pedido.getProductos());
            //registrarPedido();
            pedido = new Pedido();
            vpedido.limpiarTodo();
            mostrarNumeroPedido();
            ponerProductos();
        }
    }

    private void mostrarNumeroPedido() {
        vpedido.mostrarNumPedido(pedido.getIdPedido());
    }

    public void abrirVentanaCaja() {
        vcaja.setNombre(Sandwicheria.activo.getNombre());
        vcaja.setApellido(Sandwicheria.activo.getApellido());
        vcaja.setTurno(Sandwicheria.activo.getTurno().getDescripcion());
        vcaja.setTurno(Sandwicheria.activo.getTurno().getDescripcion());
        vcaja.setCantidadPedidos(Repo.pedidos.size());
        vcaja.setRecaudacion(modelo.calcularRecaudacion());
        
        vcaja.setVisible(true);
    }

    private void registrarPedido() {
        Autorizacion au = solicitarAutorizacion("AAE1E8F0-E693-4F5A-8A46-27C94368B492");
        System.out.println(au.getCuit()+" "+au.getSign()+" "+au.getToken());
        
    }

    private static Autorizacion solicitarAutorizacion(java.lang.String codigo) {
        org.tempuri.LoginService service = new org.tempuri.LoginService();
        org.tempuri.ILoginService port = service.getSGEAuthService();
        return port.solicitarAutorizacion(codigo);
    }
    
}
