
package sandwicheria;

import controlador.Controlador;
import modelo.Repo;

/**
 *
 * @author gabriel
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Repo.obtener();
        Controlador control = new Controlador();
        control.ejecutar();
    }
    
}
