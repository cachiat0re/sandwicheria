/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author gabriel
 */
public class Rubro {

    private ArrayList<Producto> productos = new ArrayList<Producto>();
    private String tipo;
    
    public Rubro(String tipo) {
        this.tipo = tipo;
    }

    public Rubro() {
    }

    public void agregar(Producto p){
    productos.add(p);
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }
    
    
    public String getTipo() {
        return tipo;
    }

    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
