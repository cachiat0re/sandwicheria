
package modelo;

import java.util.ArrayList;

/**
 *
 * @author gabriel
 */
public class Repo {
    
    public static ArrayList<Cajero> cajeros=new ArrayList<Cajero>();
    public static ArrayList<Rubro> rubros=new ArrayList<Rubro>();
    public static ArrayList<Producto> productos=new ArrayList<Producto>();
    public static ArrayList<Agregado> agregados=new ArrayList<Agregado>();
    public static ArrayList<Pedido> pedidos=new ArrayList<Pedido>();
    
    
    public static void obtener(){
        
        Turno t1 = new Turno("Mañana");
        Turno t2 = new Turno("Tarde");
        
        Cajero c1 = new Cajero("Gabriel","Gonzales","gabriel","12345",t1);
        Cajero c2 = new Cajero("juan","Ramirez","juan","12345",t2);
        
        cajeros.add(c1);
        cajeros.add(c2);
        
        Rubro r1 = new Rubro("Sandwich");
        Rubro r2 = new Rubro("Bebida");
        
        rubros.add(r1);
        rubros.add(r2);
        
        Producto p1 = new Producto(r1,1000,70,"Milanesa",20);
        Producto p2 = new Producto(r1,1001,80,"Lomito",0);
        r1.agregar(p1);
        r1.agregar(p2);
        Producto p3 = new Producto(r2,1002,30,"Gaseosa coca",30);
        Producto p4 = new Producto(r2,1003,30,"Gaseosa pepsi",30);
        r2.agregar(p3);
        r2.agregar(p4);
        
        productos.add(p1);
        productos.add(p2);
        productos.add(p3);
        productos.add(p4);
        
        Agregado a1 = new Agregado("Queso",20);
        Agregado a2 = new Agregado("Lechuga",0);
        Agregado a3 = new Agregado("Tomate",0);
        
        agregados.add(a1);
        agregados.add(a2);
        agregados.add(a3);
        
        
        
        
        
    }
    
}
