
package modelo;

import java.util.ArrayList;

/**
 *
 * @author gabriel
 */
public class Sandwicheria {
    public static Cajero activo;
    
    public Boolean login(String usuario, String contraseña){
        for(Cajero c:Repo.cajeros){
            if(usuario.equalsIgnoreCase(c.getUsuario())&&contraseña.equalsIgnoreCase(c.getContraseña())){
                activo=c;
                return true;
            }
        }
        
        return false;
    }

    public Producto crearProducto(Producto producto, int rubro, int producto0) {
        Rubro aux = Repo.rubros.get(rubro);
        Producto pro = aux.getProductos().get(producto0);
        if(producto == null){
            producto = new Producto(pro);
        } return producto;
    }

    public float calcularRecaudacion() {
        float total=0;
        
        for(Pedido p: Repo.pedidos){
            total+=p.calcularTotal();
        }
        return total;
    }

    public void descontarExistencias(ArrayList<Producto> productos) {
        for(Producto p: productos){
            for(Producto q:Repo.productos){
                if(p.getIdProducto()==q.getIdProducto()){
                    q.descontarExistencia();
                }
            }
        }
    }
}
