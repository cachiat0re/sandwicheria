
package modelo;

import java.util.ArrayList;

/**
 *
 * @author gabriel
 */
public class Pedido {
    
    private static int ultimo=1; //ultimo pedido
    private ArrayList<Producto> productos= new ArrayList<>();
    private int idPedido;
    private Cajero cajero;
    
    

    public Pedido() {
        idPedido=ultimo;
        ultimo++;
    }

    public Cajero getCajero() {
        return cajero;
    }

    public void setCajero(Cajero cajero) {
        this.cajero = cajero;
    }
    
    

    public static int getUltimo() {
        return ultimo;
    }

    public static void setUltimo(int ultimo) {
        Pedido.ultimo = ultimo;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }
    
    
    
    public void agregarProducto(Producto p){
        productos.add(p);
    }
    
    public float calcularTotal(){
        float total=0;
        for(Producto p: productos){
            total+=p.calcularTotal();
        }
        return total;
    }
    
    
}
