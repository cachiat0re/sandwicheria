
package modelo;

import java.util.ArrayList;

/**
 *
 * @author gabriel
 */
public class Producto{
    
    private Rubro rubro;

    private int idProducto;
    
    private float precio;
    
    private String nombre;
    
    
    
    private ArrayList<Agregado> agregados = new ArrayList<Agregado>();
    private int existencia;

    
    public Producto(Rubro rubro, int idProducto, float precio, String nombre, int existencia) {
        this.rubro = rubro;
        this.idProducto = idProducto;
        this.precio = precio;
        this.nombre = nombre;
        this.existencia=existencia;
       
    }
    
    public void agregar(Agregado ag){
        agregados.add(ag);
    }
    
    public float calcularTotal(){
        
        float total=0;
        total=precio+calcularTotalAgregados();
        return total;
        
    }
    
    
    public float calcularTotalAgregados(){
        
        float total=0;
        for(Agregado a: agregados){
            total += a.getPrecio();
        }
        
        return total;
        
    }
    
    
    //clonar producto para armar en el pedido
    public Producto(Producto p) {
        this.idProducto = p.getIdProducto();
        this.nombre=p.getNombre();
        this.precio = p.getPrecio();
        this.rubro = p.getRubro();
        this.existencia=p.getExistencia();
        
    }

    public Rubro getRubro() {
        return rubro;
    }

    public void setRubro(Rubro rubro) {
        this.rubro = rubro;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public  int getExistencia() {
        return existencia;
    }

    public  void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public ArrayList<Agregado> getAgregados() {
        return agregados;
    }

    public void setAgregados(ArrayList<Agregado> agregados) {
        this.agregados = agregados;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    public float getPrecio() {
        return precio;
    }

   
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void descontarExistencia() {
        existencia --;
    }
}
