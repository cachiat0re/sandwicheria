/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import modelo.Agregado;
import modelo.Producto;
import modelo.Rubro;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author juan
 */
public class ProductoTest {
    
    public ProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void calcularTotalAgregadosTest(){
        
        Producto p = new Producto((new Rubro()),1000,70,"Milanesa",20);
        Agregado a = new Agregado("QUESO", 10);
        p.agregar(a);
        float expected = 10;
        assertEquals(expected, p.calcularTotalAgregados(),0);
        
        
        
    }
    
    @Test
    public void calcularTotalAgregadosTest2(){
        
        Producto p = new Producto((new Rubro()),1000,70,"Milanesa",20);
        Agregado a  = new Agregado("QUESO", 10);
        Agregado a1 = new Agregado("JAMON", 14);
        Agregado a2 = new Agregado("MAYO", 15);
        Agregado a3 = new Agregado("MOSTAZA", 10);
        
        p.agregar(a);
        p.agregar(a1);
        p.agregar(a2);
        p.agregar(a3);
        
        float expected = a.getPrecio()+a1.getPrecio()+a2.getPrecio()+a3.getPrecio();
        assertEquals(expected, p.calcularTotalAgregados(),0);
        
        
        
    }
    
    @Test
    public void calcularTotalTest(){
        
        
        
        
        Producto p = new Producto((new Rubro()),1000,70,"Milanesa",20);
        
        Agregado a  = new Agregado("QUESO", 10);
        Agregado a1 = new Agregado("JAMON", 14);
        Agregado a2 = new Agregado("MAYO", 15);
        Agregado a3 = new Agregado("MOSTAZA", 10);
        
        p.agregar(a);
        p.agregar(a1);
        p.agregar(a2);
        p.agregar(a3);
        
        //float totalAgregados = p.calcularTotalAgregados();
        
        float expected = 70+10+14+15+10;
        
        assertEquals(expected, p.calcularTotal(),0);
        
        
        
    }
    
    
}
