/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import modelo.Pedido;
import modelo.Producto;
import modelo.Repo;
import modelo.Rubro;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author juan
 */
public class PedidoTest {
    
    public PedidoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void calcularTotalTest1(){
           Pedido pedido = new Pedido();
           
           
           //pedido.setProductos(Repo.productos);
           pedido.agregarProducto((new Producto((new Rubro()),1000,70,"Milanesa",20)));
           float expected=70;
           assertEquals(expected, pedido.calcularTotal(),0);
    
    }
    
    @Test
    public void calcularTotalTest2(){
           Pedido pedido = new Pedido();
           
           ArrayList<Producto> productos= new ArrayList<>();
           productos.add((new Producto((new Rubro()),1000,70,"Milanesa",20)));
           productos.add((new Producto((new Rubro()),1000,100,"Lomito",20)));
           
           pedido.setProductos(productos);
           
           float expected=170;
           assertEquals(expected, pedido.calcularTotal(),0);
           
           
    
    }
}
